'use strict';

module.exports = {
  client: {
    lib: {
      css: [
        'public/lib/bootstrap/dist/css/bootstrap.css',
        'public/lib/bootstrap/dist/css/bootstrap-theme.css',
        'public/lib/font-awesome/css/font-awesome.css',
        'public/lib/Ionicons/css/ionicons.css',
        'public/lib/ngprogress/ngProgress.css'
      ],
      js: [
        'public/lib/angular/angular.js',
        'public/lib/angular-resource/angular-resource.js',
        'public/lib/angular-sanitize/angular-sanitize.js',
        'public/lib/angular-animate/angular-animate.js',
        'public/lib/angular-messages/angular-messages.js',
        'public/lib/angular-ui-router/release/angular-ui-router.js',
        'public/lib/angular-ui-utils/ui-utils.js',
        'public/lib/angular-bootstrap/ui-bootstrap.min.js',
        'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
        'public/lib/angular-file-upload/angular-file-upload.js',
        'public/lib/momentjs/min/moment.min.js',
        'public/lib/momentjs/min/locales.min.js',
        'public/lib/humanize-duration/humanize-duration.js',
        'public/lib/angular-timer/dist/angular-timer.min.js',
        'public/lib/ngprogress/build/ngprogress.min.js'
      ],
      tests: ['public/lib/angular-mocks/angular-mocks.js']
    },
    css: [
      'modules/core/client/css/main-loading.css',
      'modules/core/client/css/ninja-slider.css',
      'modules/core/client/css/mainlandingpage.css',
      'modules/core/client/css/core.css',
      'modules/core/client/css/widget.css',
      'modules/core/client/css/sidebar.css',
      'modules/core/client/css/header.css',
      'modules/core/client/css/content.css',
      'modules/core/client/css/rightsidebar.css',
      'modules/core/client/css/message.css',
      'modules/users/client/css/login.css' ,
      'modules/users/client/css/ftu.css',
      'modules/users/client/css/users.css'
            
    ],
    less: [
      'modules/*/client/less/*.less'
    ],
    sass: [
      'modules/*/client/scss/*.scss'
    ],
    js: [
      'modules/core/client/app/config.js',
      'modules/core/client/app/init.js',
        'modules/core/client/app/ninja-slider.js',
      'modules/*/client/*.js',
      'modules/*/client/**/*.js'
    ],
    views: ['modules/*/client/views/**/*.html'],
    templates: ['build/templates.js']
  },
  server: {
    gruntConfig: 'gruntfile.js',
    gulpConfig: 'gulpfile.js',
    allJS: ['server.js', 'config/**/*.js', 'modules/*/server/**/*.js'],
    models: 'modules/*/server/models/**/*.js',
    routes: ['modules/!(core)/server/routes/**/*.js', 'modules/core/server/routes/**/*.js'],
    sockets: 'modules/*/server/sockets/**/*.js',
    config: 'modules/*/server/config/*.js',
    policies: 'modules/*/server/policies/*.js',
    views: 'modules/*/server/views/*.html'
  }
};
