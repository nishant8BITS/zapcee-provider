'use strict';

var defaultEnvConfig = require('./default');

// TODO: If We don't use openshift then please remove process.env.OPENSHIFT_MONGODB_DB_URL Section
module.exports = {
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || (process.env.OPENSHIFT_MONGODB_DB_URL ? : process.env.OPENSHIFT_MONGODB_DB_URL + 'zapcee') : '' || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/zapcee',
    options: {
      user: '',
      pass: ''
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || false
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      //stream: 'access.log'
    }
  },
  app: {
    title: defaultEnvConfig.app.title + ' - Development Environment'
  },
  facebook: {
    clientID: process.env.FACEBOOK_ID || '527329897424473',
    clientSecret: process.env.FACEBOOK_SECRET || 'c6ada7a9111e5ab819edd9b29a26c65e',
    callbackURL: '/api/auth/facebook/callback'
  },
  twitter: {
    clientID: process.env.TWITTER_KEY || 'qsfLkNH7AKjGRGPFz6APw',
    clientSecret: process.env.TWITTER_SECRET || 'Hx9im18nqqOPGWDuVaM60DEeN2jP8LM8x0G0zAv6gqs',
    callbackURL: '/api/auth/twitter/callback'
  },
  google: {
    clientID: process.env.GOOGLE_ID || '584888289437-kacnfdjtdol1tf0bnqj06jrtdr0cpmng.apps.googleusercontent.com',
    clientSecret: process.env.GOOGLE_SECRET || 'FnxiUt9yLOzF4Az5-wsHOIb-',
    callbackURL: '/api/auth/google/callback'
  },
  linkedin: {
    clientID: process.env.LINKEDIN_ID || 'exq0gnzonqve',
    clientSecret: process.env.LINKEDIN_SECRET || 'Q7Y28IZBPlzOu2IX',
    callbackURL: '/api/auth/linkedin/callback'
  },
  github: {
    clientID: process.env.GITHUB_ID || 'APP_ID',
    clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
    callbackURL: '/api/auth/github/callback'
  },
  paypal: {
    clientID: process.env.PAYPAL_ID || 'CLIENT_ID',
    clientSecret: process.env.PAYPAL_SECRET || 'CLIENT_SECRET',
    callbackURL: '/api/auth/paypal/callback',
    sandbox: true
  },
  mailer: {
    from: process.env.MAILER_FROM || 'nihantanu@gmail.com',
    options: {
      service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
      auth: {
        user: process.env.MAILER_EMAIL_ID || 'nihantanu@gmail.com',
        pass: process.env.MAILER_PASSWORD || 'iitismydream1989'
      }
    }
  },
  mailGunmailer: {
    from: process.env.MAILER_FROM || 'nihantanu@gmail.com',
    options: {
      auth: {
        api_key: 'key-e0440c78c2425590ddf8254e81167636',
        domain: 'sandbox0d89d08363114015bd9f15e3c3d2f2d6.mailgun.org'
      }
    }
  },
  sendGridmailer: {
    from: process.env.MAILER_FROM || 'nihantanu@gmail.com',
    options: {
      auth: {
        api_user: 'nihantanu',
        api_key: 'nihantanu2010'
      }
    }
  },
  livereload: true,
  seedDB: process.env.MONGO_SEED || false
};
