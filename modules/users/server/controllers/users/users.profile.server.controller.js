'use strict';
/**
 * Module dependencies.
 */
var _ = require('lodash'),
  fs = require('fs'),
  path = require('path'),
  config = require(path.resolve('./config/config')),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  mongoose = require('mongoose'),
  User = mongoose.model('User'),
  async = require('async'),
  nodemailer = require('nodemailer'),
  sg = require('nodemailer-sendgrid-transport'),
  crypto = require('crypto');

   var smtpTransport = nodemailer.createTransport(sg(config.sendGridmailer.options));
/**
 * Update user details
 */
exports.update = function(req, res) {
  // Init Variables
  var user = req.user;
  // For security measurement we remove the roles from the req.body object
  delete req.body.roles;
  if (user) {
    // Merge existing user
    user = _.extend(user, req.body);
    user.updated = Date.now();
    user.displayName = user.firstName + ' ' + user.lastName;
    user.save(function(err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        req.login(user, function(err) {
          if (err) {
            res.status(400).send(err);
          } else {
            res.json(user);
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};
/**
 * Update profile picture
 */
exports.changeProfilePicture = function(req, res) {
  var user = req.user;
  var message = null;
  if (user) {
    fs.writeFile('./modules/users/client/img/profile/uploads/' + req.files.file.name, req.files.file.buffer, function(uploadError) {
      if (uploadError) {
        return res.status(400).send({
          message: 'Error occurred while uploading profile picture'
        });
      } else {
        user.profileImageURL = 'modules/users/client/img/profile/uploads/' + req.files.file.name;
        user.save(function(saveError) {
          if (saveError) {
            return res.status(400).send({
              message: errorHandler.getErrorMessage(saveError)
            });
          } else {
            req.login(user, function(err) {
              if (err) {
                res.status(400).send(err);
              } else {
                res.json(user);
              }
            });
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: 'User is not signed in'
    });
  }
};
/**
 * Send User
 */
exports.me = function(req, res) {
  res.json(req.user || null);
};

/**
 *  Method GET : Validate Primary Email Address 
 */
exports.confirmPrimaryEmailAdd = function(req, res) {
  User.findOne({
    email: req.param.userEmail,
    emailVerificationToken: req.param.token
  }, function(err, user) {
    if (!user) {
      // Redirect to Invalid Page with Message
      return res.redirect('/provider/invalid');
    } else if (user.emailVerified) {
      console.log('Email Address is already verified');
      return res.redirect('/provider/confirmed');
    } else {
      user.emailVerified = true;
      user.save(function(err){
        if(!err){
          console.log('Thanks For verifying your email Address');
          return res.redirect('/provider/suceess');
        }else{
          console.log("Some Error occur at server for confirming email address");
        }
      });
    }
  });
};
/** 
 * Method POST : Resend Confirmation Email
 */
exports.resendEmailConfirmation = function(req, res) {
  var emailAdd = req.body.email;
  User.findOne({
    email: emailAdd
  }, function(err, user) {
    // Sent Verfication Email Address for first time signup user
    async.waterfall([
      function(done) {
        var token = user.emailVerificationToken;
        done(err, token, user);
      },
      function(token, user, done) {
        res.render(path.resolve('modules/users/server/templates/signup-email-verification-confirm'), {
          name: user.displayName,
          appName: config.app.title,
          url: 'http://' + req.headers.host + '/api/auth/confirm-email/' + user.email + '/' + token
        }, function(err, emailHTML) {
          done(err, emailHTML, user);
        });
      },
      // If valid email, send email verification mail using service
      function(emailHTML, user, done) {
        var mailOptions = {
          to: user.email,
          from: config.mailer.from,
          subject: 'Please confirm your e-mail address - Zapcee',
          html: emailHTML
        };
        smtpTransport.sendMail(mailOptions, function(err) {
          if (!err) {
            res.send({
              message: 'An email has been sent to the provided email with further instructions.'
            });
           } else {
            return res.status(400).send({
              message: 'Failure sending email'
            });
          }
          done(err);
        });
      }
    ], function(err) {
      console.log(err);
    });
  });
};