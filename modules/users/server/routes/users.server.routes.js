'use strict';

module.exports = function (app) {
  // User Routes
  var users = require('../controllers/users.server.controller');

  // Setting up the users profile api
  app.route('/api/users/me').get(users.me);
  app.route('/api/users').put(users.update);
  app.route('/api/users/accounts').delete(users.removeOAuthProvider);
  app.route('/api/users/password').post(users.changePassword);
  app.route('/api/users/picture').post(users.changeProfilePicture);

  // API For Re-Sending Email Confirmation
  app.route('/api/users/resendPrimaryEmailVerification').post(users.resendEmailConfirmation);

  // Finish by binding the user middleware
  app.param('userId', users.userByID);
};
