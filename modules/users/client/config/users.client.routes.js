'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
  function ($stateProvider) {
    // Users state routing
    $stateProvider
      .state('index.home.settings', {
        abstract: true,
        url: '/provider/settings',
        views: {
              'content': {
                  templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
              },
              'subnav@index': {
                  templateUrl: 'modules/users/client/views/settings/profileSettings.subnav.client.view.html',
              }
          },
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('index.home.settings.profile', {
          url: '/profile',
          templateUrl: 'modules/users/client/views/settings/edit-profile.client.view.html'
      })
      .state('index.home.settings.verification', {
        url: '/verification',
        templateUrl: 'modules/users/client/views/settings/verification.client.view.html'
      })
      .state('index.home.settings.review', {
        url: '/review',
        templateUrl: 'modules/users/client/views/settings/review.client.view.html'
      })
      .state('index.home.settings.picture', {
        url: '/picture',
        templateUrl: 'modules/users/client/views/settings/change-profile-picture.client.view.html'
      })
      .state('index.home.settings.reference', {
        url: '/reference',
        templateUrl: 'modules/users/client/views/settings/reference.client.view.html'
      })



      .state('index.home.account', {
        abstract: true,
        url: '/provider/account',
        views: {
              'content': {
                  templateUrl: 'modules/users/client/views/settings/settings.client.view.html',
              },
              'subnav@index': {
                  templateUrl: 'modules/users/client/views/settings/accountSettings.subnav.client.view.html',
              }
          },
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('index.home.account.notification', {
          url: '/profile',
          templateUrl: 'modules/users/client/views/settings/notification.client.view.html'
      })
      .state('index.home.account.payout', {
        url: '/payout',
        templateUrl: 'modules/users/client/views/settings/payout.client.view.html'
      })
      .state('index.home.account.transactionHist', {
        url: '/transactionHist',
        templateUrl: 'modules/users/client/views/settings/transactionHist.client.view.html'
      })
      .state('index.home.account.privacy', {
        url: '/privacy',
        templateUrl: 'modules/users/client/views/settings/privacy.client.view.html'
      })
      .state('index.home.account.security', {
        url: '/security',
        templateUrl: 'modules/users/client/views/settings/security.client.view.html'
      })
            .state('index.home.account.settings', {
        url: '/accountsettings',
        templateUrl: 'modules/users/client/views/settings/account-settings.client.view.html'
      })


      .state('auth', {
        abstract: true,
        url: '/provider/auth',
        templateUrl: 'modules/users/client/views/authentication/authentication.client.view.html'
      })
      .state('auth.signup', {
        url: '/signup',
        templateUrl: 'modules/users/client/views/authentication/signup.client.view.html'
      })
      .state('auth.signin', {
        url: '/signin?err',
        templateUrl: 'modules/users/client/views/authentication/signin.client.view.html'
      })
      .state('ftu', {
        abstract: true,
        url: '/provider/ftu',
        templateUrl: 'modules/users/client/views/ftu/ftu-home.client.view.html',
        data: {
          roles: ['user', 'admin']
        }
      })
      .state('ftu.services', {
        url: '/services',
        templateUrl: 'modules/users/client/views/ftu/services.client.view.html'
      })
      .state('password', {
        abstract: true,
        url: '/password',
        template: '<ui-view/>'
      })
      .state('password.forgot', {
        url: '/forgot',
        templateUrl: 'modules/users/client/views/password/forgot-password.client.view.html'
      })
      .state('password.reset', {
        abstract: true,
        url: '/reset',
        template: '<ui-view/>'
      })
      .state('password.reset.invalid', {
        url: '/invalid',
        templateUrl: 'modules/users/client/views/password/reset-password-invalid.client.view.html'
      })
      .state('password.reset.success', {
        url: '/success',
        templateUrl: 'modules/users/client/views/password/reset-password-success.client.view.html'
      })
      .state('password.reset.form', {
        url: '/:token',
        templateUrl: 'modules/users/client/views/password/reset-password.client.view.html'
      })
      .state('emailConfirmation', {
        url: '/provider/suceess',
        templateUrl: 'modules/users/client/views/password/email-verification-success.client.view.html'
      })
      .state('emailConfirmationInvalid', {
        url: '/provider/invalid',
        templateUrl: 'modules/users/client/views/password/invalid-email-verification.client.view.html'
      })
      .state('emailConfirmedAlready', {
        url: '/provider/confirmed',
        templateUrl: 'modules/users/client/views/password/already-verified-email-confirmation.client.view.html'
      });
  }
]);
