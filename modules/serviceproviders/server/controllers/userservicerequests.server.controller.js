'use strict';
/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	Servicerequest = mongoose.model('Servicerequest'),
	Serviceprovider = mongoose.model('Serviceprovider'),
	Broadcastlist = mongoose.model('Broadcastlist'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
/**
 * Create a Servicerequest
 */
exports.create = function(req, res) {
	var servicerequest = new Servicerequest(req.body);
	servicerequest.user = req.user;
	// res.jsonp(serviceproviders);
	servicerequest.save(function(err, servicerequestObj) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			// res.jsonp(servicerequest);
			Serviceprovider.find({
				'location.coordinates': {
					$near: {
						$geometry: {
							type: 'Point',
							coordinates: req.body.location.coordinates
						},
						// $minDistance: 1000,
						$maxDistance: 5000
					}
				}
			}).sort('-created').populate({
				path: 'serviceId',
				match: {
					serviceType: req.serviceType
				},
				select: 'serviceType serviceName serviceDescription'
			}).populate('user', 'displayName').exec(function(err, serviceproviders) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					console.log(' List of service provider ' + serviceproviders);
					var providerList = serviceproviders.map(function(provider) {
						var newProv = {};
						newProv.serviceProvider = provider._id;
						newProv.serviceRequest = servicerequestObj._id;
						return newProv;
					});
					console.log('all done ' + servicerequestObj._id);
					Broadcastlist.collection.insert(providerList, function(err) {
						if (err) {
							return res.status(400).send({
								message: errorHandler.getErrorMessage(err)
							});
						} else {
							console.log('awesome query surya');
						}
					}); // console.log(providerList);
					// res.jsonp(serviceproviders);
				}
			});
		}
	});
};
/**
 * Show the current Servicerequest
 */
exports.read = function(req, res) {
	res.jsonp(req.servicerequest);
};
/**
 * Update a Servicerequest
 */
exports.update = function(req, res) {
	var servicerequest = req.servicerequest;
	servicerequest = _.extend(servicerequest, req.body);
	servicerequest.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(servicerequest);
		}
	});
};
/**
 * Delete an Servicerequest
 */
exports.delete = function(req, res) {
	var servicerequest = req.servicerequest;
	servicerequest.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(servicerequest);
		}
	});
};
/**
 * List of Servicerequests
 */
exports.list = function(req, res) {
	Broadcastlist.find({
		'serviceProvider': req.user._id
	}).sort('-created').populate('serviceRequest').exec(function(err, servicerequests) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(servicerequests);
		}
	});
};
/**
 * Servicerequest middleware
 */
exports.userservicerequestId = function(req, res, next, id) {
	Servicerequest.findById(id).populate('user', 'displayName').exec(function(err, servicerequest) {
		if (err)
			return next(err);
		if (!servicerequest)
			return next(new Error('Failed to load Servicerequest ' + id));
		req.servicerequest = servicerequest;
		next();
	});
};