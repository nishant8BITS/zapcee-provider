'use strict';
/**
 * Module dependencies.
 */
var _ = require('lodash'),
	path = require('path'),
	mongoose = require('mongoose'),
	Serviceprovider = mongoose.model('Serviceprovider'),
	errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));
/**
 * Create a Serviceprovider
 */
exports.create = function(req, res) {
	var serviceprovider = new Serviceprovider(req.body);
	serviceprovider.user = req.user;
	serviceprovider.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(serviceprovider);
		}
	});
};
/**
 * Show the current Serviceprovider
 */
exports.read = function(req, res) {
	res.jsonp(req.serviceprovider);
};
/**
 * Update a Serviceprovider
 */
exports.update = function(req, res) {
	var serviceprovider = req.serviceprovider;
	serviceprovider = _.extend(serviceprovider, req.body);
	serviceprovider.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(serviceprovider);
		}
	});
};
/**
 * Delete an Serviceprovider
 */
exports.delete = function(req, res) {
	var serviceprovider = req.serviceprovider;
	serviceprovider.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(serviceprovider);
		}
	});
};
/**
 * List of Serviceproviders
 */
exports.list = function(req, res) {
	Serviceprovider.find({}, 'serviceId').sort('-created').populate('serviceId', 'serviceName serviceDescription serviceIcon serviceImageURL').exec(function(err, serviceproviders) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(serviceproviders);
		}
	});
};
/**
 * Serviceprovider middleware
 */
exports.serviceproviderByID = function(req, res, next, id) {
	Serviceprovider.findById(id).populate('serviceId').exec(function(err, serviceprovider) {
		if (err)
			return next(err);
		if (!serviceprovider)
			return next(new Error('Failed to load Serviceprovider ' + id));
		req.serviceprovider = serviceprovider;
		next();
	});
};