'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Serviceprovider Schema
 */
var ServiceproviderSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	serviceId: {
		type: Schema.ObjectId,
		ref: 'Service'
	},
	serviceSettings: {
		type: {}
	},
	location: {
		houseNo: {
			type: String,
			default: '',
			trim: true
		},
		street: {
			type: String,
			default: '',
			trim: true
		},
		city: {
			type: String,
			default: '',
			trim: true
		},
		zip: {
			type: String,
			default: '',
			trim: true
		},
		state: {
			type: String,
			default: '',
			trim: true
		},
		country: {
			type: String,
			default: '',
			trim: true
		},
		coordinates: {
			type: [Number], 
			index: '2dsphere'
		}
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Serviceprovider', ServiceproviderSchema);