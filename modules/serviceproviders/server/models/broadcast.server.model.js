'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Request Schema
 */
var BroadcastList = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	serviceRequest: {
		type: Schema.ObjectId,
		ref: 'Servicerequest'
	},
	status: {
		type: Number,
    	default: 0
	},
	quotes: {
		type: Number
	},
	serviceProvider: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Broadcastlist', BroadcastList);