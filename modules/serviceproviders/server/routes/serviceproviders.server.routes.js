'use strict';

module.exports = function(app) {
	var serviceproviders = require('../controllers/serviceproviders.server.controller');
	var serviceprovidersPolicy = require('../policies/serviceproviders.server.policy');

	// Serviceproviders Routes
	app.route('/api/serviceproviders').all(serviceprovidersPolicy.isAllowed)
		.get(serviceproviders.list)
		.post(serviceproviders.create);

	app.route('/api/serviceproviders/:serviceproviderId').all(serviceprovidersPolicy.isAllowed)
		.get(serviceproviders.read)
		.put(serviceproviders.update)
		.delete(serviceproviders.delete);

	// Finish by binding the Serviceprovider middleware
	app.param('serviceproviderId', serviceproviders.serviceproviderByID);
};