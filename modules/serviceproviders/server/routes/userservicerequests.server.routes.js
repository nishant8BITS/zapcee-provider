'use strict';

module.exports = function(app) {
	var serviceproviders = require('../controllers/userservicerequests.server.controller');
	var serviceprovidersPolicy = require('../policies/userservicerequests.server.policy');

	// Serviceproviders Routes
	app.route('/api/userservicerequests').all(serviceprovidersPolicy.isAllowed)
		.get(serviceproviders.list)
		.post(serviceproviders.create);

	app.route('/api/userservicerequests/:userservicerequestId').all(serviceprovidersPolicy.isAllowed)
		.get(serviceproviders.read)
		.put(serviceproviders.update)
		.delete(serviceproviders.delete);

	// Finish by binding the Serviceprovider middleware
	app.param('userservicerequestId', serviceproviders.userservicerequestId);
};