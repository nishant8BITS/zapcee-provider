'use strict';

// Configuring the Serviceproviders module
angular.module('serviceproviders').run(['Menus',
	function(Menus) {
		// Add the Serviceproviders dropdown item
		Menus.addMenuItem('topbar', {
			title: 'Serviceproviders',
			state: 'serviceproviders',
			type: 'dropdown'
		});

		// Add the dropdown list item
		Menus.addSubMenuItem('topbar', 'serviceproviders', {
			title: 'List Serviceproviders',
			state: 'serviceproviders.list'
		});

		// Add the dropdown create item
		Menus.addSubMenuItem('topbar', 'serviceproviders', {
			title: 'Create Serviceprovider',
			state: 'serviceproviders.create'
		});
	}
]);