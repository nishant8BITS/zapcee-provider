'use strict';

//Setting up route
angular.module('serviceproviders').config(['$stateProvider',
	function($stateProvider) {
		// Serviceproviders state routing
		$stateProvider.
		state('serviceproviders', {
			abstract: true,
			url: '/serviceproviders',
			template: '<ui-view/>'
		}).
		state('serviceproviders.list', {
			url: '',
			templateUrl: 'modules/serviceproviders/client/views/list-serviceproviders.client.view.html'
		}).
		state('serviceproviders.create', {
			url: '/create',
			templateUrl: 'modules/serviceproviders/client/views/create-serviceprovider.client.view.html'
		}).
		state('serviceproviders.view', {
			url: '/:serviceproviderId',
			templateUrl: 'modules/serviceproviders/client/views/view-serviceprovider.client.view.html'
		}).
		state('serviceproviders.edit', {
			url: '/:serviceproviderId/edit',
			templateUrl: 'modules/serviceproviders/client/views/edit-serviceprovider.client.view.html'
		});
	}
]);