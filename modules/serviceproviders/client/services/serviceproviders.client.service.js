'use strict';

//Serviceproviders service used to communicate Serviceproviders REST endpoints
angular.module('serviceproviders').factory('Serviceproviders', ['$resource',
	function($resource) {
		return $resource('api/serviceproviders/:serviceproviderId', { serviceproviderId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);