'use strict';
// Serviceproviders controller
angular.module('serviceproviders').controller('ServiceprovidersController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Serviceproviders',
  function ($scope, $stateParams, $location, Authentication, Serviceproviders) {
    $scope.authentication = Authentication;
    // Create new Serviceprovider
    $scope.create = function () {
      // Create new Serviceprovider object
      var serviceprovider = new Serviceproviders({
        // name: this.name
        serviceId: '55ec6b01c2a59df86187c9bd',
        location: {
          'coordinates': [
            33.377796,
            35.480911
          ]
        }
      });
      // Redirect after save
      serviceprovider.$save(function (response) {
        $location.path('serviceproviders/' + response._id);  // Clear form fields
                                                             // $scope.name = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Serviceprovider
    $scope.remove = function (serviceprovider) {
      if (serviceprovider) {
        serviceprovider.$remove();
        for (var i in $scope.serviceproviders) {
          if ($scope.serviceproviders[i] === serviceprovider) {
            $scope.serviceproviders.splice(i, 1);
          }
        }
      } else {
        $scope.serviceprovider.$remove(function () {
          $location.path('serviceproviders');
        });
      }
    };
    // Update existing Serviceprovider
    $scope.update = function () {
      var serviceprovider = $scope.serviceprovider;
      serviceprovider.$update(function () {
        $location.path('serviceproviders/' + serviceprovider._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Serviceproviders
    $scope.find = function () {
      $scope.serviceproviders = Serviceproviders.query();
      console.log($scope.serviceproviders);
    };
    // Find existing Serviceprovider
    $scope.findOne = function () {
      $scope.serviceprovider = Serviceproviders.get({ serviceproviderId: $stateParams.serviceproviderId });
      console.log($scope.serviceprovider);
    };
  }
]);