'use strict';

(function() {
	// Serviceproviders Controller Spec
	describe('Serviceproviders Controller Tests', function() {
		// Initialize global variables
		var ServiceprovidersController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Serviceproviders controller.
			ServiceprovidersController = $controller('ServiceprovidersController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Serviceprovider object fetched from XHR', inject(function(Serviceproviders) {
			// Create sample Serviceprovider using the Serviceproviders service
			var sampleServiceprovider = new Serviceproviders({
				name: 'New Serviceprovider'
			});

			// Create a sample Serviceproviders array that includes the new Serviceprovider
			var sampleServiceproviders = [sampleServiceprovider];

			// Set GET response
			$httpBackend.expectGET('api/serviceproviders').respond(sampleServiceproviders);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.serviceproviders).toEqualData(sampleServiceproviders);
		}));

		it('$scope.findOne() should create an array with one Serviceprovider object fetched from XHR using a serviceproviderId URL parameter', inject(function(Serviceproviders) {
			// Define a sample Serviceprovider object
			var sampleServiceprovider = new Serviceproviders({
				name: 'New Serviceprovider'
			});

			// Set the URL parameter
			$stateParams.serviceproviderId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/api\/serviceproviders\/([0-9a-fA-F]{24})$/).respond(sampleServiceprovider);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.serviceprovider).toEqualData(sampleServiceprovider);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Serviceproviders) {
			// Create a sample Serviceprovider object
			var sampleServiceproviderPostData = new Serviceproviders({
				name: 'New Serviceprovider'
			});

			// Create a sample Serviceprovider response
			var sampleServiceproviderResponse = new Serviceproviders({
				_id: '525cf20451979dea2c000001',
				name: 'New Serviceprovider'
			});

			// Fixture mock form input values
			scope.name = 'New Serviceprovider';

			// Set POST response
			$httpBackend.expectPOST('api/serviceproviders', sampleServiceproviderPostData).respond(sampleServiceproviderResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Serviceprovider was created
			expect($location.path()).toBe('/serviceproviders/' + sampleServiceproviderResponse._id);
		}));

		it('$scope.update() should update a valid Serviceprovider', inject(function(Serviceproviders) {
			// Define a sample Serviceprovider put data
			var sampleServiceproviderPutData = new Serviceproviders({
				_id: '525cf20451979dea2c000001',
				name: 'New Serviceprovider'
			});

			// Mock Serviceprovider in scope
			scope.serviceprovider = sampleServiceproviderPutData;

			// Set PUT response
			$httpBackend.expectPUT(/api\/serviceproviders\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/serviceproviders/' + sampleServiceproviderPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid serviceproviderId and remove the Serviceprovider from the scope', inject(function(Serviceproviders) {
			// Create new Serviceprovider object
			var sampleServiceprovider = new Serviceproviders({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Serviceproviders array and include the Serviceprovider
			scope.serviceproviders = [sampleServiceprovider];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/api\/serviceproviders\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleServiceprovider);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.serviceproviders.length).toBe(0);
		}));
	});
}());