'use strict';

var should = require('should'),
	request = require('supertest'),
	path = require('path'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Serviceprovider = mongoose.model('Serviceprovider'),
	express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, serviceprovider;

/**
 * Serviceprovider routes tests
 */
describe('Serviceprovider CRUD tests', function() {
	before(function(done) {
		// Get application
		app = express.init(mongoose);
		agent = request.agent(app);

		done();
	});

	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Serviceprovider
		user.save(function() {
			serviceprovider = {
				name: 'Serviceprovider Name'
			};

			done();
		});
	});

	it('should be able to save Serviceprovider instance if logged in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Serviceprovider
				agent.post('/api/serviceproviders')
					.send(serviceprovider)
					.expect(200)
					.end(function(serviceproviderSaveErr, serviceproviderSaveRes) {
						// Handle Serviceprovider save error
						if (serviceproviderSaveErr) done(serviceproviderSaveErr);

						// Get a list of Serviceproviders
						agent.get('/api/serviceproviders')
							.end(function(serviceprovidersGetErr, serviceprovidersGetRes) {
								// Handle Serviceprovider save error
								if (serviceprovidersGetErr) done(serviceprovidersGetErr);

								// Get Serviceproviders list
								var serviceproviders = serviceprovidersGetRes.body;

								// Set assertions
								(serviceproviders[0].user._id).should.equal(userId);
								(serviceproviders[0].name).should.match('Serviceprovider Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Serviceprovider instance if not logged in', function(done) {
		agent.post('/api/serviceproviders')
			.send(serviceprovider)
			.expect(403)
			.end(function(serviceproviderSaveErr, serviceproviderSaveRes) {
				// Call the assertion callback
				done(serviceproviderSaveErr);
			});
	});

	it('should not be able to save Serviceprovider instance if no name is provided', function(done) {
		// Invalidate name field
		serviceprovider.name = '';

		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Serviceprovider
				agent.post('/api/serviceproviders')
					.send(serviceprovider)
					.expect(400)
					.end(function(serviceproviderSaveErr, serviceproviderSaveRes) {
						// Set message assertion
						(serviceproviderSaveRes.body.message).should.match('Please fill Serviceprovider name');
						
						// Handle Serviceprovider save error
						done(serviceproviderSaveErr);
					});
			});
	});

	it('should be able to update Serviceprovider instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Serviceprovider
				agent.post('/api/serviceproviders')
					.send(serviceprovider)
					.expect(200)
					.end(function(serviceproviderSaveErr, serviceproviderSaveRes) {
						// Handle Serviceprovider save error
						if (serviceproviderSaveErr) done(serviceproviderSaveErr);

						// Update Serviceprovider name
						serviceprovider.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Serviceprovider
						agent.put('/api/serviceproviders/' + serviceproviderSaveRes.body._id)
							.send(serviceprovider)
							.expect(200)
							.end(function(serviceproviderUpdateErr, serviceproviderUpdateRes) {
								// Handle Serviceprovider update error
								if (serviceproviderUpdateErr) done(serviceproviderUpdateErr);

								// Set assertions
								(serviceproviderUpdateRes.body._id).should.equal(serviceproviderSaveRes.body._id);
								(serviceproviderUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Serviceproviders if not signed in', function(done) {
		// Create new Serviceprovider model instance
		var serviceproviderObj = new Serviceprovider(serviceprovider);

		// Save the Serviceprovider
		serviceproviderObj.save(function() {
			// Request Serviceproviders
			request(app).get('/api/serviceproviders')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Serviceprovider if not signed in', function(done) {
		// Create new Serviceprovider model instance
		var serviceproviderObj = new Serviceprovider(serviceprovider);

		// Save the Serviceprovider
		serviceproviderObj.save(function() {
			request(app).get('/api/serviceproviders/' + serviceproviderObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', serviceprovider.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Serviceprovider instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Serviceprovider
				agent.post('/api/serviceproviders')
					.send(serviceprovider)
					.expect(200)
					.end(function(serviceproviderSaveErr, serviceproviderSaveRes) {
						// Handle Serviceprovider save error
						if (serviceproviderSaveErr) done(serviceproviderSaveErr);

						// Delete existing Serviceprovider
						agent.delete('/api/serviceproviders/' + serviceproviderSaveRes.body._id)
							.send(serviceprovider)
							.expect(200)
							.end(function(serviceproviderDeleteErr, serviceproviderDeleteRes) {
								// Handle Serviceprovider error error
								if (serviceproviderDeleteErr) done(serviceproviderDeleteErr);

								// Set assertions
								(serviceproviderDeleteRes.body._id).should.equal(serviceproviderSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Serviceprovider instance if not signed in', function(done) {
		// Set Serviceprovider user 
		serviceprovider.user = user;

		// Create new Serviceprovider model instance
		var serviceproviderObj = new Serviceprovider(serviceprovider);

		// Save the Serviceprovider
		serviceproviderObj.save(function() {
			// Try deleting Serviceprovider
			request(app).delete('/api/serviceproviders/' + serviceproviderObj._id)
			.expect(403)
			.end(function(serviceproviderDeleteErr, serviceproviderDeleteRes) {
				// Set message assertion
				(serviceproviderDeleteRes.body.message).should.match('User is not authorized');

				// Handle Serviceprovider error error
				done(serviceproviderDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec(function(){
			Serviceprovider.remove().exec(function(){
				done();
			});
		});
	});
});
