'use strict';

// Configuring the Services module
angular.module('services').run(['Menus',
	function(Menus) {
		// Add the Services dropdown item
		Menus.addMenuItem('topbar', {
			title: 'Services',
			state: 'services',
			type: 'dropdown'
		});

		// Add the dropdown list item
		Menus.addSubMenuItem('topbar', 'services', {
			title: 'List Services',
			state: 'services.list'
		});

		// Add the dropdown create item
		Menus.addSubMenuItem('topbar', 'services', {
			title: 'Create Service',
			state: 'services.create'
		});
	}
]);