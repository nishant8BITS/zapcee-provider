'use strict';

module.exports = function(app) {
	var services = require('../controllers/services.server.controller');
	var servicesPolicy = require('../policies/services.server.policy');

	// Services Routes
	app.route('/api/services').all()
		.get(services.list).all(servicesPolicy.isAllowed)
		.post(services.create);

	app.route('/api/services/:serviceId').all(servicesPolicy.isAllowed)
		.get(services.read)
		.put(services.update)
		.delete(services.delete);

	// Finish by binding the Service middleware
	app.param('serviceId', services.serviceByID);
};