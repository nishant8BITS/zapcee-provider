'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Request Schema
 */
var BroadcastList = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	// broadCastList: {
	// 	type: [{
	// 		provider: {
	// 			type: Schema.ObjectId,
	// 			ref: 'User'
	// 		},
	// 		acceptedByProvider: {
	// 			type: Boolean,
	// 			default: false
	// 		}
	// 	}]
	// },
	// 0: Approved, 1:Responded
	status: {
		type: Number,
    	default: 0
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Broadcast', BroadcastList);