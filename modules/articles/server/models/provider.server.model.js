'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Request Schema
 */
var ProviderSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	serviceList: {
		type: {}
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Provider', ProviderSchema);