'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Service Schema
 */
var ServiceSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	serviceType: {
		type: String,
		required: 'Please fill Service type',
		trim: true
	},
	serviceName: {
		type: String,
		required: 'Please fill Service Name',
		trim: true
	},
	serviceDescription: {
		type: String,
		required: 'Please fill Service Description',
		trim: true
	},
	serviceIcon: {
		type: String,
		required: 'Please fill Service Icon',
		trim: true
	},
	serviceImageURL: {
	    type: String,
	    default: ''
	    // default: 'modules/users/client/img/profile/default.png'
 	},
});

mongoose.model('ServiceList', ServiceSchema);