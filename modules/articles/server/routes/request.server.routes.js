'use strict';

/**
 * Module dependencies.
 */
var articlesPolicy = require('../policies/request.server.policy'),
  request = require('../controllers/request.server.controller');

module.exports = function (app) {
  // Articles collection routes
  app.route('/api/request').all(articlesPolicy.isAllowed)
    .get(request.list)
    .post(request.create);

  // Single article routes
  app.route('/api/request/:articleId').all(articlesPolicy.isAllowed)
    .get(request.read)
    .put(request.update)
    .delete(request.delete);

  // Finish by binding the article middleware
  app.param('articleId', request.articleByID);
};
