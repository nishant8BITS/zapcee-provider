'use strict';


angular.module('core').factory('MessageBox', ['Authentication', '$state', '$timeout',
  function (Authentication, $state, $timeout) {
    var messageBoxList = [],
        limit = 2;

    return {
      newMessageBox : newMessageBox,
      removeMessageBox : removeMessageBox
    };

    function newMessageBox( id ) {
      if(!arrayContains(id)) {
        messageBoxList.push(id);
      }
    }

    function removeMessageBox() {

    }

    //helper method
    function getParticularMessageBox() {

    }

    function arrayContains(id) {
      var i,
          len;
      for (i = 0,len = messageBoxList.length; i < len; i++) {
        if (messageBoxList[i].id === id) {
          return true;
        }
      }
      return false;
    }

  }
]);
