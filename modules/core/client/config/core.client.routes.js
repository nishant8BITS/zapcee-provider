'use strict';
// Setting up route
angular.module('core').config([
  '$stateProvider',
  '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
    // Take user to landing page ex: www.zapcee.com
    $urlRouterProvider.when('', '/');
    //Redirect to 404 when route not found
    $urlRouterProvider.otherwise(function($injector, $location) {
      $injector.get('$state').transitionTo('not-found', null, {
        location: false
      });
    });
    /*
       Zapcee Main Landing Page 
     */
    $stateProvider.state('home', {
        url: '/',
        templateUrl: 'modules/core/client/views/mainLandingPage.client.view.html',
        controller: 'MainLandingPageCtrl as mlpc'
      }) // Provider Landing page 
      .state('provider', {
        url: '/provider',
        templateUrl: 'modules/core/client/views/providerLandingPage.client.view.html',
        controller: 'ProviderLandingPageCtrl as plpc'
      });
    // Home state routing
    $stateProvider.state('index', {
      abstract: true,
      templateUrl: 'modules/core/client/views/layout.client.view.html'
    }).state('index.home', {
      abstract: true,
      views: {
        'topnav': {
          templateUrl: '/modules/core/client/views/partials/home.topnav.client.view.html'
        },
        'sidebar': {
          templateUrl: '/modules/core/client/views/partials/home.sidebar.client.view.html'
        },
        'subnav': {
          templateUrl: '/modules/core/client/views/partials/home.subnav.client.view.html'
        },
        'main': {
          templateUrl: '/modules/core/client/views/partials/home.maincontent.client.view.html'
        }
      }
    }).state('index.home.request', {
      url: '/provider/request',
      views: {
        'content@index.home': {
          templateUrl: '/modules/core/client/views/partials/contentTemp.html'
        }
      }
    }).state('index.home.respond', {
      url: '/provider/respond',
      views: {
        'content@index.home': {
          templateUrl: '/modules/core/client/views/partials/responded.html'
        }
      }
    }).state('not-found', {
      url: '/not-found',
      templateUrl: 'modules/core/client/views/404.client.view.html',
      data: {
        ignoreState: true
      }
    }).state('bad-request', {
      url: '/bad-request',
      templateUrl: 'modules/core/client/views/400.client.view.html',
      data: {
        ignoreState: true
      }
    }).state('forbidden', {
      url: '/forbidden',
      templateUrl: 'modules/core/client/views/403.client.view.html',
      data: {
        ignoreState: true
      }
    });
  }
]);