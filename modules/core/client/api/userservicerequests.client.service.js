'use strict';

//Servicerequests service used to communicate Servicerequests REST endpoints
angular.module('core').factory('UserServiceRequests', ['$resource',
	function($resource) {
		return $resource('api/userservicerequests/:userservicerequestId', { userservicerequestId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);