'use strict';
angular.module('core').controller('SidebarController', [
  '$scope',
  'Authentication',
  'Serviceproviders',
  function ($scope, Authentication, Serviceproviders) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    // Find a list of services provided by this Serviceproviders
    $scope.find = function () {
      $scope.services = Serviceproviders.query();
      console.log($scope.serviceproviders);
    };
    $scope.alerts = [{
        type: 'danger',
        msg: 'Oh snap! Change a few things up and try submitting again.'
      }];
    $scope.closeAlert = function (index) {
      $scope.alerts.splice(index, 1);
    };  // Find existing Serviceprovider
        // $scope.findOne = function() {
        // 	$scope.serviceprovider = Serviceproviders.get({ 
        // 		serviceproviderId: $stateParams.serviceproviderId
        // 	});
        // 	console.log($scope.serviceprovider);
        // };
        // $scope.services = [{ serviceName : 'Car Rental' , serviceIcon: 'fa fa-car' , notification: 1}];
  }
]);