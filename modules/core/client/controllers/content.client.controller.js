'use strict';
angular.module('core').controller('ContentController', [
  '$scope',
  'Authentication',
  'UserServiceRequests',
  '$modal',
  '$sce',
  function ($scope, Authentication, UserServiceRequests, $modal, $sce) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope._timeLeft = [];
    $scope.userServiceRequests = '';
    // helper function :: move it to utility services 
    var formatDate = function (time) {
      var minutes = time;
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);
      hours = hours - days * 24;
      minutes = minutes - days * 24 * 60 - hours * 60;
      return {
        days: days,
        hours: hours,
        minutes: minutes
      };
    };
    var initTimer = function () {
      var intervalID = setInterval(function () {
        var len, i, fDate;
        for (i = 0, len = $scope.userServiceRequests.length; i < len; i++) {
          if ($scope._timeLeft[i].timeLeft === 0) {
            continue;
          }
          $scope._timeLeft[i].timeLeft -= 1;
          $scope._timeLeft[i].fDate = formatDate($scope._timeLeft[i].timeLeft);
        }
        $scope.$apply();
      }, 60000);
    };
    // Find a list of services provided by this Serviceproviders
    $scope.find = function () {
      UserServiceRequests.query({}, function (response) {
        var i, len;
        $scope.userServiceRequests = response;
        for (i = 0, len = $scope.userServiceRequests.length; i < len; i++) {
          $scope._timeLeft[i] = {};
          $scope._timeLeft[i].timeLeft = 52 * i;
          $scope._timeLeft[i].fDate = formatDate($scope._timeLeft[i].timeLeft);
        }
        initTimer();
      });
      console.log($scope.userServiceRequests);
    };
    $scope.toggle = function (item) {
      item.expanded = !item.expanded;
    };
    $scope.openMessageBox = function (e) {
      var xPos = e.clientX, yPos = e.clientY, elem = document.getElementById('chat-box');
      xPos = xPos + document.body.scrollLeft + document.documentElement.scrollLeft - e.offsetX - 818;
      yPos = yPos + document.body.scrollTop + document.documentElement.scrollTop - e.offsetY - 280;
      elem.style.left = xPos + 'px';
      elem.style.top = yPos + 'px';
      angular.element(elem).removeClass('hidden');
    };
    $scope.closeMessageBox = function () {
      angular.element(document.getElementById('chat-box')).addClass('hidden');
    };
    // modal related function
    $scope.animationsEnabled = true;
    $scope.items = [
      'item1',
      'item2',
      'item3'
    ];
    $scope.open = function (size) {
      var modalInstance = $modal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'modules/core/client/views/partials/home.providerquotes.client.view.html',
        controller: 'ModalInstanceCtrl',
        size: size,
        windowClass: 'app-modal-window',
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
      });
    };
    $scope.toggleAnimation = function () {
      $scope.animationsEnabled = !$scope.animationsEnabled;
    };
    // pop
    $scope.dynamicPopover = {
      content: 'place?q=place_id:ChIJLbZ-NFv9DDkRzk0gTkm3wlI',
      templateUrl: 'modules/core/client/views/partials/popover.template.html',
      title: 'Title'
    };
    $scope.direction = $sce.trustAsResourceUrl('https://www.google.com/maps/embed/v1/place?q=delhi&key=AIzaSyDHWv2anO1y-h3y3rtnrKmJviZGH2IVMUM');
  }
]);
angular.module('core').controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {
  $scope.items = items;
  $scope.selected = { item: $scope.items[0] };
  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});