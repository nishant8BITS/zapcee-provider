'use strict';
angular.module('core').controller('HomeController', [
  '$scope',
  '$state',
  'Authentication',
  function ($scope, $state, Authentication) {
    // This provides Authentication context.
    $scope.authentication = Authentication;
    $scope.rightBar = false;
    $scope.messageBox = false;
    $scope.onClickNotification = function () {
      $scope.rightBar = true;
    };
    $scope.onClickMessage = function () {
      $scope.rightBar = true;
    };
    $scope.closeRightBar = function () {
      $scope.rightBar = false;
    };
    $scope.toggleMessageBox = function () {
      $scope.messageBox = !$scope.messageBox;
    };
  }
]);

