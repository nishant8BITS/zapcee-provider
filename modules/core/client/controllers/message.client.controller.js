'use strict';
angular.module('core').controller('MessageController', [
  '$scope',
  '$state',
  'Authentication',
  'MessageBox',
  function ($scope, $state, Authentication, MessageBox) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;
  }
]);