'use strict';
angular.module('core').controller('HeaderController', [
  '$scope',
  '$state',
  'Authentication',
  'Menus',
  '$http',
  function ($scope, $state, Authentication, Menus,$http) {
    // Expose view variables
    $scope.$state = $state;
    $scope.authentication = Authentication;
    $scope.isEmailVerified = true;

    // Check whether email is verified or not show message based on that
    if (!Authentication.user.emailVerified) {
       $scope.isEmailVerified = false;

      // Show Bootstrap UI alert box
      $scope.alert = {
        type: 'danger',
        msg: 'Please verify your email address ' 
      };
    }

    $scope.resendVerificationEmail = function(){
      $http.post('/api/users/resendPrimaryEmailVerification', {'emailAdd': Authentication.user.email}).success(function (response) {
        $scope.alert = {
          type: 'success',
          msg: 'We have re-sent you verifcation email at' + Authentication.user.email + ' Still didn\'t get email don\'t worry '  
        };
      }).error(function (response) {
       console.log("Some Error occur");
      });
    }

    // Get the topbar menu
    $scope.menu = Menus.getMenu('topbar');
    // Toggle the menu items
    $scope.isCollapsed = false;
    $scope.toggleCollapsibleMenu = function () {
      $scope.isCollapsed = !$scope.isCollapsed;
    };
    // Collapsing the menu after navigation
    $scope.$on('$stateChangeSuccess', function () {
      $scope.isCollapsed = false;
    });
  }
]);