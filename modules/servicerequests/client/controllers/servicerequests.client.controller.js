'use strict';
// Servicerequests controller
angular.module('servicerequests').controller('ServicerequestsController', [
  '$scope',
  '$stateParams',
  '$location',
  'Authentication',
  'Servicerequests',
  function ($scope, $stateParams, $location, Authentication, Servicerequests) {
    $scope.authentication = Authentication;
    // Create new Servicerequest
    $scope.create = function () {
      // Create new Servicerequest object
      var servicerequest = new Servicerequests({
        location: {
          'coordinates': [
            33.377796,
            35.480911
          ]
        },
        quotes: '33',
        serviceType: 'Car'
      });
      // Redirect after save
      servicerequest.$save(function (response) {
        $location.path('servicerequests/' + response._id);
        // Clear form fields
        $scope.name = '';
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Remove existing Servicerequest
    $scope.remove = function (servicerequest) {
      if (servicerequest) {
        servicerequest.$remove();
        for (var i in $scope.servicerequests) {
          if ($scope.servicerequests[i] === servicerequest) {
            $scope.servicerequests.splice(i, 1);
          }
        }
      } else {
        $scope.servicerequest.$remove(function () {
          $location.path('servicerequests');
        });
      }
    };
    // Update existing Servicerequest
    $scope.update = function () {
      var servicerequest = $scope.servicerequest;
      servicerequest.$update(function () {
        $location.path('servicerequests/' + servicerequest._id);
      }, function (errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
    // Find a list of Servicerequests
    $scope.find = function () {
      $scope.servicerequests = Servicerequests.query();
    };
    // Find existing Servicerequest
    $scope.findOne = function () {
      $scope.servicerequest = Servicerequests.get({ servicerequestId: $stateParams.servicerequestId });
    };
  }
]);