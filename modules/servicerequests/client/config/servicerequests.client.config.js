'use strict';
// Configuring the Servicerequests module
angular.module('servicerequests').run([
	'Menus',
	function(Menus) {
		// Add the Servicerequests dropdown item
		Menus.addMenuItem('topbar', {
			title: 'Servicerequests',
			state: 'servicerequests',
			type: 'dropdown'
		});
		// Add the dropdown list item
		Menus.addSubMenuItem('topbar', 'servicerequests', {
			title: 'List Servicerequests',
			state: 'servicerequests.list'
		});
		// Add the dropdown create item
		Menus.addSubMenuItem('topbar', 'servicerequests', {
			title: 'Create Servicerequest',
			state: 'servicerequests.create'
		});
	}
]);