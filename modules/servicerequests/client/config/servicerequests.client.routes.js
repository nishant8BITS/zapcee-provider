'use strict';
//Setting up route
angular.module('servicerequests').config([
	'$stateProvider',
	function($stateProvider) {
		// Servicerequests state routing
		$stateProvider.state('index.home.servicerequests', {
			abstract: true,
			url: '/servicerequests',
			template: '<ui-view/>'
		}).state('index.home.servicerequests.list', {
			url: '',
			views: {
				'content@index.home': {
					templateUrl: 'modules/servicerequests/client/views/list-servicerequests.client.view.html'
				}
			}
		}).state('index.home.servicerequests.create', {
			url: '/create',
			views: {
				'content@index.home': {
					templateUrl: 'modules/servicerequests/client/views/create-servicerequest.client.view.html'
				}
			}
		}).state('index.home.servicerequests.view', {
			url: '/:servicerequestId',
			views: {
				'content@index.home': {
					templateUrl: 'modules/servicerequests/client/views/view-servicerequest.client.view.html'
				}
			}
		}).state('index.home.servicerequests.edit', {
			url: '/:servicerequestId/edit',
			templateUrl: 'modules/servicerequests/client/views/edit-servicerequest.client.view.html'
		});
	}
]);