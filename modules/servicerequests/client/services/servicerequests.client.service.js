'use strict';

//Servicerequests service used to communicate Servicerequests REST endpoints
angular.module('servicerequests').factory('Servicerequests', ['$resource',
	function($resource) {
		return $resource('api/servicerequests/:servicerequestId', { servicerequestId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);