'use strict';

var should = require('should'),
	request = require('supertest'),
	path = require('path'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Servicerequest = mongoose.model('Servicerequest'),
	express = require(path.resolve('./config/lib/express'));

/**
 * Globals
 */
var app, agent, credentials, user, servicerequest;

/**
 * Servicerequest routes tests
 */
describe('Servicerequest CRUD tests', function() {
	before(function(done) {
		// Get application
		app = express.init(mongoose);
		agent = request.agent(app);

		done();
	});

	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Servicerequest
		user.save(function() {
			servicerequest = {
				name: 'Servicerequest Name'
			};

			done();
		});
	});

	it('should be able to save Servicerequest instance if logged in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Servicerequest
				agent.post('/api/servicerequests')
					.send(servicerequest)
					.expect(200)
					.end(function(servicerequestSaveErr, servicerequestSaveRes) {
						// Handle Servicerequest save error
						if (servicerequestSaveErr) done(servicerequestSaveErr);

						// Get a list of Servicerequests
						agent.get('/api/servicerequests')
							.end(function(servicerequestsGetErr, servicerequestsGetRes) {
								// Handle Servicerequest save error
								if (servicerequestsGetErr) done(servicerequestsGetErr);

								// Get Servicerequests list
								var servicerequests = servicerequestsGetRes.body;

								// Set assertions
								(servicerequests[0].user._id).should.equal(userId);
								(servicerequests[0].name).should.match('Servicerequest Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Servicerequest instance if not logged in', function(done) {
		agent.post('/api/servicerequests')
			.send(servicerequest)
			.expect(403)
			.end(function(servicerequestSaveErr, servicerequestSaveRes) {
				// Call the assertion callback
				done(servicerequestSaveErr);
			});
	});

	it('should not be able to save Servicerequest instance if no name is provided', function(done) {
		// Invalidate name field
		servicerequest.name = '';

		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Servicerequest
				agent.post('/api/servicerequests')
					.send(servicerequest)
					.expect(400)
					.end(function(servicerequestSaveErr, servicerequestSaveRes) {
						// Set message assertion
						(servicerequestSaveRes.body.message).should.match('Please fill Servicerequest name');
						
						// Handle Servicerequest save error
						done(servicerequestSaveErr);
					});
			});
	});

	it('should be able to update Servicerequest instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Servicerequest
				agent.post('/api/servicerequests')
					.send(servicerequest)
					.expect(200)
					.end(function(servicerequestSaveErr, servicerequestSaveRes) {
						// Handle Servicerequest save error
						if (servicerequestSaveErr) done(servicerequestSaveErr);

						// Update Servicerequest name
						servicerequest.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Servicerequest
						agent.put('/api/servicerequests/' + servicerequestSaveRes.body._id)
							.send(servicerequest)
							.expect(200)
							.end(function(servicerequestUpdateErr, servicerequestUpdateRes) {
								// Handle Servicerequest update error
								if (servicerequestUpdateErr) done(servicerequestUpdateErr);

								// Set assertions
								(servicerequestUpdateRes.body._id).should.equal(servicerequestSaveRes.body._id);
								(servicerequestUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Servicerequests if not signed in', function(done) {
		// Create new Servicerequest model instance
		var servicerequestObj = new Servicerequest(servicerequest);

		// Save the Servicerequest
		servicerequestObj.save(function() {
			// Request Servicerequests
			request(app).get('/api/servicerequests')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Servicerequest if not signed in', function(done) {
		// Create new Servicerequest model instance
		var servicerequestObj = new Servicerequest(servicerequest);

		// Save the Servicerequest
		servicerequestObj.save(function() {
			request(app).get('/api/servicerequests/' + servicerequestObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', servicerequest.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Servicerequest instance if signed in', function(done) {
		agent.post('/api/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Servicerequest
				agent.post('/api/servicerequests')
					.send(servicerequest)
					.expect(200)
					.end(function(servicerequestSaveErr, servicerequestSaveRes) {
						// Handle Servicerequest save error
						if (servicerequestSaveErr) done(servicerequestSaveErr);

						// Delete existing Servicerequest
						agent.delete('/api/servicerequests/' + servicerequestSaveRes.body._id)
							.send(servicerequest)
							.expect(200)
							.end(function(servicerequestDeleteErr, servicerequestDeleteRes) {
								// Handle Servicerequest error error
								if (servicerequestDeleteErr) done(servicerequestDeleteErr);

								// Set assertions
								(servicerequestDeleteRes.body._id).should.equal(servicerequestSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Servicerequest instance if not signed in', function(done) {
		// Set Servicerequest user 
		servicerequest.user = user;

		// Create new Servicerequest model instance
		var servicerequestObj = new Servicerequest(servicerequest);

		// Save the Servicerequest
		servicerequestObj.save(function() {
			// Try deleting Servicerequest
			request(app).delete('/api/servicerequests/' + servicerequestObj._id)
			.expect(403)
			.end(function(servicerequestDeleteErr, servicerequestDeleteRes) {
				// Set message assertion
				(servicerequestDeleteRes.body.message).should.match('User is not authorized');

				// Handle Servicerequest error error
				done(servicerequestDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec(function(){
			Servicerequest.remove().exec(function(){
				done();
			});
		});
	});
});
