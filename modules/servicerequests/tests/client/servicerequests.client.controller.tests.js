'use strict';

(function() {
	// Servicerequests Controller Spec
	describe('Servicerequests Controller Tests', function() {
		// Initialize global variables
		var ServicerequestsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Servicerequests controller.
			ServicerequestsController = $controller('ServicerequestsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Servicerequest object fetched from XHR', inject(function(Servicerequests) {
			// Create sample Servicerequest using the Servicerequests service
			var sampleServicerequest = new Servicerequests({
				name: 'New Servicerequest'
			});

			// Create a sample Servicerequests array that includes the new Servicerequest
			var sampleServicerequests = [sampleServicerequest];

			// Set GET response
			$httpBackend.expectGET('api/servicerequests').respond(sampleServicerequests);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.servicerequests).toEqualData(sampleServicerequests);
		}));

		it('$scope.findOne() should create an array with one Servicerequest object fetched from XHR using a servicerequestId URL parameter', inject(function(Servicerequests) {
			// Define a sample Servicerequest object
			var sampleServicerequest = new Servicerequests({
				name: 'New Servicerequest'
			});

			// Set the URL parameter
			$stateParams.servicerequestId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/api\/servicerequests\/([0-9a-fA-F]{24})$/).respond(sampleServicerequest);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.servicerequest).toEqualData(sampleServicerequest);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Servicerequests) {
			// Create a sample Servicerequest object
			var sampleServicerequestPostData = new Servicerequests({
				name: 'New Servicerequest'
			});

			// Create a sample Servicerequest response
			var sampleServicerequestResponse = new Servicerequests({
				_id: '525cf20451979dea2c000001',
				name: 'New Servicerequest'
			});

			// Fixture mock form input values
			scope.name = 'New Servicerequest';

			// Set POST response
			$httpBackend.expectPOST('api/servicerequests', sampleServicerequestPostData).respond(sampleServicerequestResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Servicerequest was created
			expect($location.path()).toBe('/servicerequests/' + sampleServicerequestResponse._id);
		}));

		it('$scope.update() should update a valid Servicerequest', inject(function(Servicerequests) {
			// Define a sample Servicerequest put data
			var sampleServicerequestPutData = new Servicerequests({
				_id: '525cf20451979dea2c000001',
				name: 'New Servicerequest'
			});

			// Mock Servicerequest in scope
			scope.servicerequest = sampleServicerequestPutData;

			// Set PUT response
			$httpBackend.expectPUT(/api\/servicerequests\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/servicerequests/' + sampleServicerequestPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid servicerequestId and remove the Servicerequest from the scope', inject(function(Servicerequests) {
			// Create new Servicerequest object
			var sampleServicerequest = new Servicerequests({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Servicerequests array and include the Servicerequest
			scope.servicerequests = [sampleServicerequest];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/api\/servicerequests\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleServicerequest);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.servicerequests.length).toBe(0);
		}));
	});
}());