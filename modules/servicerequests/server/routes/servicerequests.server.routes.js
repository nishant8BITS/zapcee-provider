'use strict';

module.exports = function(app) {
	var servicerequests = require('../controllers/servicerequests.server.controller');
	var servicerequestsPolicy = require('../policies/servicerequests.server.policy');

	// Servicerequests Routes
	app.route('/api/servicerequests').all()
		.get(servicerequests.list).all(servicerequestsPolicy.isAllowed)
		.post(servicerequests.create);

	app.route('/api/servicerequests/:servicerequestId').all(servicerequestsPolicy.isAllowed)
		.get(servicerequests.read)
		.put(servicerequests.update)
		.delete(servicerequests.delete);

	// Finish by binding the Servicerequest middleware
	app.param('servicerequestId', servicerequests.servicerequestByID);
};