'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Servicerequest Schema
 */
var ServicerequestSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	// serviceDate: {
	// 	type: Date,
	// 	required: 'Please fill Service type',
	// },
	serviceType: {
		type: String,
		default: '',
		required: 'Please fill Service type',
		trim: true
	},
	quotes: {
		type: Number,
		required: 'Please select the Quotes'
	},
	location: {
		houseNo: {
			type: String,
			default: '',
			trim: true
		},
		street: {
			type: String,
			default: '',
			trim: true
		},
		city: {
			type: String,
			default: '',
			trim: true
		},
		zip: {
			type: String,
			default: '',
			trim: true
		},
		state: {
			type: String,
			default: '',
			trim: true
		},
		country: {
			type: String,
			default: '',
			trim: true
		},
		coordinates: {
			type: [Number], 
			index: '2dsphere'
		}
	},
	status: {
		type: Number,
    	default: 0
	},
	acceptByUser: {
		type: {}
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Servicerequest', ServicerequestSchema);